package ooss;

import java.util.Objects;

public class Student extends Person {
    protected Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Student student = (Student) o;

        return Objects.equals(klass, student.klass);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (klass != null ? klass.hashCode() : 0);
        return result;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (this.klass == null) return false;
        return this.klass.number == klass.number;
    }

    public String introduce() {

        StringBuilder introduce = new StringBuilder(String.format("My name is %s. I am %d years old. I am a student.", this.name, this.age));
        if (this.klass != null && !klass.isLeader(this)) {
            introduce.append(String.format(" I am in class %d.", this.klass.number));
        } else if (this.klass != null && klass.isLeader(this)) {
            introduce.append(String.format(" I am the leader of class %d.", this.klass.number));
        }
        return introduce.toString();
    }

    public void update(Student leader){
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", name, leader.klass.number,leader.name));
    }
}
