package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {
    protected List<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klasses = new ArrayList<>();
    }


    public String introduce() {
        StringBuffer introduce = new StringBuffer(String.format("My name is %s. I am %d years old. I am a teacher.", this.name, this.age));
        if (klasses.size() > 0) {
            introduce.append(" I teach Class");
        }
        for (int i = 0; i < klasses.size(); i++) {
            Klass klass = klasses.get(i);
            if (i == klasses.size() - 1) {
                introduce.append(String.format(" %d.", klass.number));
            } else {
                introduce.append(String.format(" %d,", klass.number));
            }
        }
        return introduce.toString();
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return this.klasses.contains(student.klass);
    }
    public void update(Student leader){
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", name,leader.klass.number,leader.name));
    }
}
