package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    protected int number;
    protected Student leader;
    protected List<Person> people;

    public Klass(int number) {
        this.number = number;
        this.people = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Klass klass = (Klass) o;

        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return number;
    }

    public void assignLeader(Student student) {
        if (student.klass != null && this.number == student.klass.number) {
            this.leader = student;
            notifyAllObservers();
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        if(leader==null)return false;
        return leader==student;
    }

    public void attach(Person person) {
        this.people.add(person);
    }

    private void notifyAllObservers(){
        this.people.forEach(person -> person.update(this.leader));
    }

}
